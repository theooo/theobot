# theobot

My first Telegram bot. Doesn't do really useful stuff. Just to learn and have fun and stuff.  

Try it out at: [https://t.me/theo_testbot](https://t.me/theo_testbot)

## Dependencies

- Python
- [Pyrogram](https://github.com/pyrogram/pyrogram)
- [Requests](https://pypi.org/project/requests/)
- [jq.py](https://github.com/mwilliamson/jq.py)
