from pyrogram import Client, filters
import requests as rq
import jq
from textwrap import dedent
import re

bot = Client("theobot")

@bot.on_message(filters.command('rbq'))
def random(client, message):
    quote = rq.get("https://labs.bible.org/api/?passage=random").text
    bot.send_message(message.chat.id, quote)

@bot.on_message(filters.command('pokemon'))
def pokemon(client, message):
    try:
        pokemon_character = message.text.split()[1]
    except IndexError:
        message.reply("Please pass in pokemon name with command.")
    else:
        response = rq.get(f"https://pokeapi.co/api/v2/pokemon/{pokemon_character}")
        if response.status_code == 200:
            data = response.text
            link = jq.first('.sprites.other."official-artwork".front_default', text=data)
            message.reply_photo(link)
        else:
            message.reply("Not Found.")

@bot.on_message(filters.command('dict'))
def meaning(client, message):
    try:
        word = message.text.split()[1]
    except IndexError:
        message.reply("Please pass in word with the command.")
    else:
        response = rq.get(f"https://api.dictionaryapi.dev/api/v2/entries/en/{word}")
        if response.status_code == 200:
            data = response.text
            meanings = jq.all(".[].meanings[].definitions[].definition", text=data)
            answer = "<b>Meanings</b>\n"
            for count, item in enumerate(meanings, start=1):
                answer += f'{count}) {item}\n'
            message.reply(answer)
        else:
            message.reply("Not Found.")

@bot.on_message(filters.command('xkcd'))
def xkcd(client, message):
    resp = rq.get("https://c.xkcd.com/random/comic/")
    link = re.search('imgs.xkcd.com.*png', resp.text).group()
    message.reply_photo("https://" + link, caption=resp.url)

@bot.on_message(filters.command('bruh'))
def bruh(client, message):
    bruhtext = """<code>
    ██████╗ ██████╗ 
    ██╔══██╗██╔══██╗
    ██████╔╝██████╔╝
    ██╔══██╗██╔══██╗
    ██████╔╝██║  ██║
    ╚═════╝ ╚═╝  ╚═╝
    ██╗   ██╗██╗  ██╗
    ██║   ██║██║  ██║
    ██║   ██║███████║
    ██║   ██║██╔══██║
    ╚██████╔╝██║  ██║
     ╚═════╝ ╚═╝  ╚═╝
        </code>
        """
    
    message.reply(bruhtext)

@bot.on_message(filters.command(['help','start']))
def help(client, message):
    helptext = """
    /help - print this help message.
    /bruh - send bruh ASCII.
    /dict - send the meanings of a word.
    /pokemon - send the image of a pokemon named {name}.
    /rbq - send a random bible verse.
    /xkcd - send a random xkcd comic.
    """
    
    message.reply(dedent(helptext))

bot.run()
